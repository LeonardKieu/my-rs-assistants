import pymongo
from pymongo import MongoClient

def print_a_dict(d):
    for k, v in d.items():
        print("{}: {}".format(k, v))
        if k == '_id':
            print(type(v))
        print("----------------------------------------------------------")