import pymongo
from pymongo import MongoClient
import preprocess

def process_author():
    # Read collection
    list_dict = clean_coll.find({'author': {'$exists': True}})
    for d in list_dict:
        # Preprocess each dict
        raw_author = d['author']
        if type(raw_author[0]) is dict:
            clean_author = []
            for a in raw_author:
                clean_author.append(a['name'])

            # Update
            myquery = {"_id": d["_id"]}
            newvalues = {"$set": {"author": clean_author}}
            clean_coll.update_one(myquery, newvalues)

            # Test a doc in DB
            # print(d['_id'])
            # break

def process_review():
    # Read collection
    list_dict = clean_coll.find({'review': {'$exists': True}})
    for d in list_dict:
        # Preprocess each dict
        raw_review = d['review']
        if type(raw_review) is not str:
            clean_review = raw_review[0]['detailReview']

            # Update
            myquery = {"_id": d["_id"]}
            newvalues = {"$set": {"review": clean_review}}
            clean_coll.update_one(myquery, newvalues)

            # Test a doc in DB
            # print(d['_id'])
            # break

def process_fee():
    """ migrate value from RawDB to CleanDB"""
    # Read collection
    list_dict = raw_coll.find({'fee': {'$exists': True}})
    for d in list_dict:
        # Preprocess each dict
        raw_fee = d['fee']

        # Update
        myquery = {"url": d["url"]}
        newvalues = {"$set": {"fee": raw_fee}}
        res = clean_coll.update_one(myquery, newvalues)

        # Test a doc in DB
        # print(d['url'])
        # print("Update result, modified count: {}".format(res.modified_count))
        # break

if __name__ == "__main__":
    # Args (should from commandline arg)
    raw_db_arg = 'RawDB'
    raw_coll_arg = 'course_program_edx'
    clean_db_arg = 'CleanDB'
    clean_coll_arg = 'elearning_program_edx'

    # Instantiate connection to MongoDB Server via ssh tunnel
    mongoDB_uri = 'mongodb://%s:%s@%s' % ('adminDB', 'admin123', "localhost:27018")
    client = MongoClient(mongoDB_uri)
    raw_coll = client[raw_db_arg][raw_coll_arg]
    clean_coll = client[clean_db_arg][clean_coll_arg]

    # Process
    # process_author()
    # process_review()
    # process_fee()