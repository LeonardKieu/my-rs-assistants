from django.apps import AppConfig


class NotiappConfig(AppConfig):
    name = 'notiapp'
