import asyncio
from ariadne import SubscriptionType, make_executable_schema
from ariadne.asgi import GraphQL
print("This is module name of noti graphql: {}".format(__name__))
from .models import User, Event
# from notiapp.models import User, Event


type_def = """
    type Query {
        _unused: Boolean
    }

    type Subscription {
        get_notifications(email: String!): Notification
    }
    
    type Notification{
        title: String
        time: String
    }
"""

subscription = SubscriptionType()


@subscription.source("get_notifications")
async def get_notifications_generator(obj, info, email):
    user_last_login = User.objects.get(username=email).last_login

    while True:
        for new_event_for_user in Event.objects.filter(time__gte=user_last_login):
            yield {
                "title": new_event_for_user.title,
                "time": str(new_event_for_user.time)
            }
            await asyncio.sleep(1)


@subscription.field("get_notifications")
def get_notifications_resolver(event, info, email):
    return event


schema = make_executable_schema(type_def, subscription)
app = GraphQL(schema, debug=True)
