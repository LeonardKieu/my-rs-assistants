from django.db import models
from datetime import datetime

print("This is module name of models: {}".format(__name__))
# Create your models here.
class Event(models.Model):
    title = models.CharField(max_length=50, unique=True)
    time = models.DateTimeField(blank=True)

    @classmethod
    def create(cls, title: str, time: str = '04/03/20 13:55:26'):
        Event(title = title, time = datetime.strptime(time, '%m/%d/%y %H:%M:%S')).save()

    def __str__(self):
        return "{} at {}".format(self.title, self.time)

    class Meta:
        db_table = 'event_demo'


class User(models.Model):
    username = models.CharField(max_length=50, unique=True)
    last_login = models.DateTimeField(blank=True, null=True)

    @classmethod
    def create(cls, username: str, last_login: str = '04/03/20 13:55:26'):
        User(username=username, last_login=datetime.strptime(last_login, '%m/%d/%y %H:%M:%S')).save()

    def __str__(self):
        return "{} last login: {}".format(self.username, self.last_login)

    class Meta:
        db_table = 'user_demo'

# from notiapp.models import Event, User
# Event.objects.all()
# Event.create("forex trading", '04/13/20 13:55:26').save()
# User.objects.all()