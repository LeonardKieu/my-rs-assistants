import predictionio
import pandas as pd
from scipy import sparse


class PIO_Ambassador:
    list_event_types = ['spendTime', 'rate', 'redirect']

    def __init__(self,
                 access_key="Bkjpi89-mgzHH-XNXsnda2XJz8BssPof-AJ0-tlvVNYx0dVszovjRka6wnkTrXBm",
                 host='localhost'):
        self.event_clients = {}
        for event_type in PIO_Ambassador.list_event_types:
            self.event_clients[event_type] = predictionio.EventClient(
                access_key=access_key,
                channel=event_type,
                url='http://' + host + ':7070',
                threads=5,
                qsize=500
            )
        self.engine_client = predictionio.EngineClient(url="http://" + host + ":8000")

    def see_event(self):
        for event_type in PIO_Ambassador.list_event_types:
            le = self.event_clients[event_type].get_events()
            # See the newest event before
            print("Behold {} events in channel {}".format(len(le), event_type))
            for e in le[::-1]:
                print(e)

    def import_from_csv(self, filepath="ml-latest-small/ratings.csv", interval_qty_log = 1000):
        df = pd.read_csv(filepath)
        imported_rows = 0

        for index, row in df.iterrows():
            nan_mask = row.isnull()
            for event_type in PIO_Ambassador.list_event_types:
                if nan_mask[event_type] == False:
                    self.event_clients[event_type].create_event(
                        event=event_type,
                        entity_type="user",
                        entity_id=row['entityID'],
                        target_entity_type="item",
                        target_entity_id=row['targetEntityID'],
                        properties={"implicit_value": float(row[event_type])}
                    )
                    imported_rows += 1
                    if imported_rows % interval_qty_log == 0:
                        print("Imported {} records".format(imported_rows))

    def import_from_npz(self, interval_qty_log = 1000):
        imported_rows = 0
        for event_type in PIO_Ambassador.list_event_types:
            event_matrix = sparse.load_npz("./random_sparse_matrix/{}.npz".format(event_type)).tocsr()
            for i in range(event_matrix.shape[0]):
                for j in range(event_matrix.shape[1]):
                    if event_matrix[i, j] != 0:
                        self.event_clients[event_type].create_event(
                            event=event_type,
                            entity_type="user",
                            entity_id=i,
                            target_entity_type="item",
                            target_entity_id=j,
                            properties={"implicit_value": float(event_matrix[i, j])}
                        )
                        imported_rows += 1
                        if imported_rows % interval_qty_log == 0:
                            print("Imported {} records".format(imported_rows))
    def recommend(self, user="0", num=5):
        rec_lst = self.engine_client.send_query({"user": user, "num": num})
        print("For user {}".format(user))
        for item in rec_lst['itemScores']:
            print(item)

    def generate_pseudo_implicit_dataset(self, shape = (100,500)):
        for event_type in PIO_Ambassador.list_event_types:
            sparse.save_npz("./random_sparse_matrix/" + event_type, sparse.random(shape[0], shape[1], density=0.25))


if __name__ == "__main__":
    x = PIO_Ambassador()
    # x.see_event()
    # for user in range(1,11):
    #     x.recommend(user=str(user))
    # x.generate_pseudo_implicit_dataset()
    # x.import_from_npz()
    x.recommend()
