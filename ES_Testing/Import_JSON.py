from __future__ import print_function

from builtins import list
from os.path import dirname, basename, abspath
from datetime import datetime
import logging
import sys
import argparse
import pymongo
from pymongo import MongoClient

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError
from elasticsearch.helpers import bulk, streaming_bulk


def gen_data(raw_lst_dict, index_name):
    """
    Add several metadata into each json doc to index ES Server
    """
    for i in range(len(raw_lst_dict)):
        # Prepare returned obj - a dict in Py - a JSON document in ES
        yielded_dict = raw_lst_dict[i]
        # Add metadata to index into ES
        yielded_dict["_id"] = str(yielded_dict["_id"])
        yielded_dict["_index"] = index_name
        yielded_dict["_type"] = "_doc"
        yielded_dict["_op_type"] = "index"

        yield yielded_dict


def print_a_dict(d):
    for k, v in d.items():
        print("{}: {}".format(k, v))
        if k == '_id':
            print(type(v))
        print("-------------------------------------------")

if __name__ == "__main__":

    # Instantiate connection to ES Server
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-H",
        "--host",
        action="store",
        default="https://localhost:9201", # param
        # default="localhost:9200",
        # default="https://localhost:9200",
        help="The elasticsearch host you wish to connect to. (Default: localhost:9200)",

    )
    args = parser.parse_args()
    es = Elasticsearch(args.host, use_ssl=False, verify_certs=False)

    # Instantiate connection to MongoDB Server via ssh tunnel
    mongoDB_uri = 'mongodb://%s:%s@%s' % ('adminDB', 'admin123', "localhost:27018") # param for staging
    # mongoDB_uri = 'mongodb://%s:%s@%s' % (r'dbAdmin', r'db@admin', "localhost:27018")  # param for product
    client = MongoClient(mongoDB_uri)
    db = 'ProductDB' # param
    sync_colls = [
                  # 'elearning_program_coursera', #
                  # 'elearning_program_edx', #
                  # 'elearning_course_coursera', #
                  # 'elearning_course_edx', #
                  # 'elearning_course_datacamp', #
                  # 'elearning_course_futurelearn',
                  # 'elearning_program_futurelearn',
                  # 'youtube_playlist', # failed
                  # 'youtube_channel', #
                  #   'book_general_amazon', #
                    'book_science_amazon', #
                  #   'book_science_technical_amazon', #
                  #   'book_technical_amazon', #
                  ] # param

    # Index into ES
    for sync_coll in sync_colls:
        collection = client[db][sync_coll]
        # lst_dict = list(course_coll.find())
        # generated_dict = list(gen_data(lst_dict, 'course'))[0]
        # print_a_dict(generated_dict)
        list_dict = list(collection.find())
        bulk(es, gen_data(list_dict, sync_coll), stats_only=True, request_timeout=120000)