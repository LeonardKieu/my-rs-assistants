from __future__ import print_function

from os.path import dirname, basename, abspath
from datetime import datetime
import logging
import sys
import argparse
import pandas as pd
import numpy as np
from pandas import compat 

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError
from elasticsearch.helpers import bulk, streaming_bulk

# Place files csv need to be imported in same folder with this script, adjust DNS (line 44, 45) run this script!

def gen_data(raw_JSON, index_name):
    for i in range(len(raw_JSON)):
        # Prepare returned obj - a dict in Py - a JSON document in ES
        yielded_dict = raw_JSON[i]
        # Add metadata to index into ES
        yielded_dict["_index"] = index_name
        yielded_dict["_id"] = i
        yielded_dict["_type"] = "_doc"
        yielded_dict["_op_type"] = "index"
        yield yielded_dict

if __name__ == "__main__":
    # Full import
    # List file csv in current folder, list indices name in ES
    list_csv = ['_Resources -  Courses.csv', '_Resources - Youtube.csv', '_Resources - Book.csv', '_Resources - FBGroup.csv',
                '_Resources - Web.csv', '_Resources -  Company.csv', '_Resources - Slide.csv', '_Resources - Linkedln.csv',
                '_Resources - Misc.csv', 'ITViecJobs.xlsx - Sheet1.csv']
    list_indices_name = ['r_courses', 'r_youtube', 'r_books', 'r_fbgroups',
                        'r_webs', 'r_companies', 'r_slides', 'r_linkedln',
                        'r_misc', 'company_itviec']
    # partial import for testing
    # list_csv = ['_Resources - FBGroup.csv']
    # list_indices_name = ['r_fbgroups']
    # Instantiate connection to ES Server
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-H",
        "--host",
        action="store",
        default="localhost:9200",
        # default="https://search-universe-haapzyz4fwnpiuieivyovpdyma.ap-southeast-1.es.amazonaws.com/",
        help="The elasticsearch host you wish to connect to. (Default: localhost:9200)",
    )
    args = parser.parse_args()
    es = Elasticsearch(args.host)

    # Index each csv index
    for i in range(len(list_csv)):
        df = pd.read_csv(list_csv[i])
        dropnan_df = df.apply(lambda x : x.dropna().to_dict(), axis=1)

        bulk(es, gen_data(dropnan_df, list_indices_name[i]), stats_only=True)    