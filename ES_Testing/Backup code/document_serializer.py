from rest_framework.fields import CharField, IntegerField, ListField, FloatField, BooleanField
from rest_framework.serializers import Serializer


class Program(Serializer):
    title_program = CharField()
    url_program = CharField()


class Channel(Serializer):
    title_channel = CharField()
    url_channel = CharField()


class Document(Serializer):
    id = CharField(required=False)
    title = CharField(required=False)
    category = CharField(required=False)
    type = CharField(required=False)
    author = ListField(CharField, required=False)
    keyword = ListField(CharField, required=False)
    level = ListField(CharField, required=False)  # Hasn't exist
    level_owner = ListField(CharField, required=False)  # Temporary using it because level hasn't exist
    quality = FloatField(required=False)
    language = ListField(CharField, required=False)
    description = CharField(required=False)
    review = CharField(required=False)
    url = CharField(required=False)
    doc_img = CharField(required=False)
    views = IntegerField(required=False)
    created_day = CharField(required=False)
    time = CharField(required=False)
    follow = CharField(required=False)
    resource = CharField(required=False)
    member = CharField(required=False)
    fee = CharField(required=False)
    offered = ListField(CharField, required=False)
    program = Program(required=False)
    channel = Channel(required=False)
    playlist_total = IntegerField(required=False)
    publisher = CharField(required=False)
    publication_date = CharField(required=False)
    like = IntegerField(required=False)
    dislike = IntegerField(required=False)


# Get all keys of Document into list
list_returned_keys = list(Document().get_initial().keys())


class Bucket(Serializer):
    bucket_name = CharField(required=False)
    doc_count = IntegerField(required=False)


class Facet1Lv(Serializer):
    facet_name = CharField(required=False)
    sum_buckets = IntegerField(required=False)
    buckets = Bucket(many=True, required=False)


class Facet2Lv(Serializer):
    facet_name = CharField(required=False)
    facets_1lv = Facet1Lv(many=True, required=False)


class Facet(Serializer):
    facets_1lv = Facet1Lv(many=True, required=False)
    facet_2lv = Facet2Lv(many=False, required=False)


class FacetSelection(Serializer):
    facet_name = CharField(required=False)
    buckets = ListField(CharField, required=False)


class SortSelection(Serializer):
    feature = CharField(required=False)
    descending = BooleanField(required=False)


class SearchResult(Serializer):
    total_docs = IntegerField(required=True)
    matched_docs = Document(many=True, required=True)
    facets = Facet(many=False, required=False)


class TripleSearchResult(Serializer):
    best_quality_docs = Document(many=True, required=False)
    newest_docs = Document(many=True, required=False)
    most_viewed_docs = Document(many=True, required=False)
