import re
from document.serializer.document_serializer import *
from elasticsearch_dsl import Search, connections
from django.conf import settings
from datetime import datetime
from .dto import dsl_dto

import sys, json
def log(text = None):
    print(text, file=sys.stderr)

connections.configure(**settings.ELASTICSEARCH_DSL)


class DocumentSeeker:
    hash_map_category_types = { # in data
        "E_LEARNING": ["COURSE", "PROGRAM"],
        "BOOK": ["GENERAL", "SCI_TECH", "SCIENCE", "TECHNICAL"],
        "YOUTUBE": ["PLAYLISTS", "CHANNEL"]
    }
    hash_map_facet_name_2_esindex_name = {
        "category": {
            "E_LEARNING": "elearning",
            "BOOK": "book",
            "YOUTUBE": "youtube"
        },
        "type": {
            "COURSE": "course",
            "PROGRAM": "program",

            "GENERAL": "general",
            "SCI_TECH": "science_technical",
            "SCIENCE": "science",
            "TECHNICAL": "technical",

            "PLAYLISTS": "playlist",
            "CHANNEL": "channel"
        }
    }
    facet_kind = {
        "Terms": [
            # CATEGORY, TYPE in Facet 2 Lv
            "LANGUAGE", "SKILL_LEVEL"
        ],
        "Percentiles": ["RATING"],
        "Date histogram": ["RELEASE_DATE"]
    }
    facets_generator = {
        # Common mandatory keys
        "CATEGORY": {
            "terms": {
                "field": "enum_category.keyword",
                "size": 20
            }
        },
        "TYPE": {
            "terms": {
                "field": "enum_type.keyword",
                "size": 50
            }
        },
        "SKILL_LEVEL": {
            "terms": {
                "field": "enum_level_owner.keyword",
                "size": 5
            }
        },
        "RATING": {
            "percentiles": {
                "field": "quality",
                "percents": [0, 100]
            }
        },
        "LANGUAGE": {
            "terms": {
                "field": "enum_language.keyword",
                "size": 5
            }
        },
        "RELEASE_DATE": {
            "date_histogram": {
                "field": "created_day",
                "calendar_interval": "1M",
                "format": "MM-dd-yyyy"
            }
        }
    }

    def __init__(self,
                 search_text="*",
                 category="*", selected_type="*",
                 from_index=0, size=10,
                 selected_facets={}, sort_selections=[]):
        # Receive from args
        self.search_text = search_text
        self.category = category
        self.selected_type = selected_type
        self.from_index = from_index
        self.size = size
        self.selected_facets = selected_facets
        self.sort_selections = sort_selections

        # Process to build more properties
        self.body = {
            "_source": list_returned_keys,
            "from": from_index, "size": size,
            "query": {
                "function_score": {
                    # stage 1
                    "query": {
                        # Whether it exists a search text?
                    },
                    # stage 2
                    "functions": [
                        # 3 funcs (5, 10, regex 1000) here if it exists search text
                        # Process FS: Filter facet 1 lv (facet 2 lv relating index scope)
                    ],
                    "score_mode": "multiply",
                    "boost_mode": "multiply",
                    "min_score": 1
                }
            }
            # Generate Facets by aggs
            # Sort
        }
        self.main_query = {
            "multi_match": {
                "query": search_text,
                "analyzer": "my_english_analyzer",
                "fields": [
                    # Display on FE
                    "title",
                    "category",
                    "category.keyword",
                    "type",
                    "type.keyword",
                    "author",
                    "author.keyword",
                    "keyword",
                    "keyword.keyword",
                    "level",
                    "level.keyword",
                    "level_owner",
                    "level_owner.keyword",
                    "language",
                    "description",
                    "review",
                    "resource",
                    "resource.keyword",
                    "offered",
                    "publisher",
                    "publisher.keyword",
                    "title_program",
                    "title_channel",
                    # Related keys
                    # elearning_program_coursera
                    "knowledge",
                    "search_key",
                    "skill",
                    # elearning_program_edx
                    "jobOutLook",
                    # elearning_course_edx
                    "prerequisites",
                    "associated_programs.name",
                    "subject.name",
                ]
            }
        }
        self.key_sets_5 = {
            "filter": {
                "multi_match": {
                    "query": search_text,
                    "analyzer": "my_english_analyzer",
                    "fields": [
                        "keyword",
                        "author",
                        "level",
                        "level_owner",
                        "language"
                    ]
                }
            },
            "weight": 5
        }
        self.key_sets_10 = {
            "filter": {
                "multi_match": {
                    "query": search_text,
                    "analyzer": "my_english_analyzer",
                    "fields": [
                        "category",
                        "type",
                        "title",
                        "resource",
                        "offered"
                    ]
                }
            },
            "weight": 10
        }
        self.title_regex = {
            "filter": {
                "regexp": {
                    "title.keyword": {
                        "value": ".*" + re.escape(search_text) + ".*",
                        "flags": "ALL"
                    }
                }
            },
            "weight": 1000
        }

    def __process_search_result(self, search_result):
        def type_to_doc_count(type):
            for bucket in search_result['aggregations'].to_dict()["TYPE"]["buckets"]:
                if bucket["key"] == type: return bucket["doc_count"]

        def category_to_doc_count(category):
            for bucket in search_result['aggregations'].to_dict()["CATEGORY"]["buckets"]:
                if bucket["key"] == category: return bucket["doc_count"]

        # Process for list docs
        list_docs = []
        for hit in search_result['hits']['hits']:
            doc_id = hit['_id']
            hit = hit['_source']
            doc = {}
            # Get key if it exists in hit and bind into doc
            for key in list_returned_keys:
                if hasattr(hit, key):
                    doc[key] = hit[key]
            doc['id'] = doc_id
            list_docs.append(doc)
        # Process for facets
        facets = {
            "facet_2lv": {
                "facet_name": "CATEGORY",
                "facets_1lv": []
            },
            "facets_1lv": []
        }
        if 'aggregations' in search_result:
            # Process Facet1Lv
            for facet_name, facet_val in search_result['aggregations'].to_dict().items():
                # # Process facet "Term"
                if facet_name in DocumentSeeker.facet_kind["Terms"]:
                    facet = Facet1Lv()
                    setattr(facet, 'facet_name', facet_name)
                    buckets = []
                    for raw_bucket in list(facet_val['buckets']):
                        bucket = Bucket()
                        setattr(bucket, 'bucket_name', raw_bucket['key'])
                        setattr(bucket, 'doc_count', raw_bucket['doc_count'])
                        buckets.append(bucket)
                    setattr(facet, "buckets", buckets)
                    facets["facets_1lv"].append(facet)
                # # Process facet "Percentiles"
                elif facet_name in DocumentSeeker.facet_kind["Percentiles"]:
                    facet = Facet1Lv()
                    setattr(facet, 'facet_name', facet_name)
                    buckets = []
                    for bucket_name, val in dict(facet_val['values']).items():
                        bucket = Bucket()
                        setattr(bucket, 'bucket_name', "START" if "0.0" == bucket_name else "STOP")
                        setattr(bucket, 'doc_count', val)
                        buckets.append(bucket)
                    setattr(facet, "buckets", buckets)
                    facets["facets_1lv"].append(facet)
                # # Process facet "Date histogram"
                elif facet_name in DocumentSeeker.facet_kind["Date histogram"]:
                    facet = Facet1Lv()
                    setattr(facet, 'facet_name', facet_name)
                    buckets = []
                    raw_buckets = [
                        ["LAST_MONTH", 0],
                        ["LAST_YEAR", 0],
                        ["LAST_3_YEARS", 0],
                        ["LAST_5_YEARS", 0]
                    ]
                    today = datetime.now()
                    for bucket in facet_val["buckets"]:
                        day_in_past = datetime.strptime(bucket["key_as_string"], '%m-%d-%Y')
                        difference_days = (today - day_in_past).days
                        doc_count = bucket["doc_count"]
                        if difference_days <= 30:
                            raw_buckets[0][1] += doc_count
                            raw_buckets[1][1] += doc_count
                            raw_buckets[2][1] += doc_count
                            raw_buckets[3][1] += doc_count
                        elif difference_days <= 365:
                            raw_buckets[1][1] += doc_count
                            raw_buckets[2][1] += doc_count
                            raw_buckets[3][1] += doc_count
                        elif difference_days <= 365 * 3:
                            raw_buckets[2][1] += doc_count
                            raw_buckets[3][1] += doc_count
                        elif difference_days <= 365 * 5:
                            raw_buckets[3][1] += doc_count
                    # # # Convert raw_buckets to Facet1Lv
                    for raw_bucket in raw_buckets:
                        bucket = Bucket()
                        setattr(bucket, "bucket_name", raw_bucket[0])
                        setattr(bucket, "doc_count", raw_bucket[1])
                        buckets.append(bucket)
                    setattr(facet, "buckets", buckets)
                    facets["facets_1lv"].append(facet)
            # Process Facet2Lv
            hash_map_category_types = DocumentSeeker.hash_map_category_types.copy()
            list_existed_categories = [bucket["key"] for bucket in
                                       search_result['aggregations'].to_dict()["CATEGORY"]["buckets"]]
            list_existed_types = [bucket["key"] for bucket in
                                  search_result['aggregations'].to_dict()["TYPE"]["buckets"]]
            for business_category in DocumentSeeker.hash_map_category_types.keys():
                if business_category not in list_existed_categories:
                    hash_map_category_types.pop(business_category, None)
                else:
                    for business_type in DocumentSeeker.hash_map_category_types[business_category]:
                        if business_type not in list_existed_types:
                            hash_map_category_types[business_category].remove(business_type)
            # # # Convert to Facet 2 lv
            for category in hash_map_category_types.keys():
                facet = Facet1Lv()
                setattr(facet, "facet_name", category)
                buckets = []
                for type in hash_map_category_types[category]:
                    bucket = Bucket()
                    setattr(bucket, "bucket_name", type)
                    setattr(bucket, "doc_count", type_to_doc_count(type))
                    buckets.append(bucket)
                setattr(facet, "buckets", buckets)
                setattr(facet, "sum_buckets", category_to_doc_count(category))
                facets["facet_2lv"]["facets_1lv"].append(facet)
        return list_docs, facets

    def __build_basic_body(self):
        if self.search_text == "*":
            self.body['query']['function_score']['query'] = {"match_all": {}}
        else:
            self.body['query']['function_score']['query'] = self.main_query
            self.body['query']['function_score']['functions'].insert(0, self.key_sets_5)
            self.body['query']['function_score']['functions'].insert(0, self.key_sets_10)
            self.body['query']['function_score']['functions'].insert(0, self.title_regex)

    def __add_facets_generator_into_body(self):
        self.body['aggs'] = DocumentSeeker.facets_generator

    def __add_facet_demands(self):
        """
        add facet demand into body with facets 1 lv\n
        return: indices wildcard for determining which indices will be searched on ES
        """

        def facet_name_2_field(facet_name):
            if "terms" in DocumentSeeker.facets_generator[facet_name]:
                return DocumentSeeker.facets_generator[facet_name]['terms']['field']
            elif "percentiles" in DocumentSeeker.facets_generator[facet_name]:
                return DocumentSeeker.facets_generator[facet_name]['percentiles']['field']
            elif "date_histogram" in DocumentSeeker.facets_generator[facet_name]:
                return DocumentSeeker.facets_generator[facet_name]["date_histogram"]["field"]
            return None

        def facet_name_2_facet_kind(facet_name):
            for kind in DocumentSeeker.facet_kind.keys():
                if facet_name in DocumentSeeker.facet_kind[kind]: return kind
            return None

        list_selected_facets_1lv = self.selected_facets.get("facets_1lv", [])
        list_selected_facets_in_facet_2lv = self.selected_facets.get("facet_2lv", [])
        # Process facet 1 lv
        for selection in list_selected_facets_1lv:
            if "Terms" == facet_name_2_facet_kind(selection["facet_name"]):
                facet_condition = {
                    "filter": {
                        "bool": {
                            "must_not": {
                                "bool": {
                                    "should": [],
                                    "minimum_should_match": 1
                                }
                            }
                        }
                    },
                    "weight": 0
                }
                for bucket in selection['buckets']:
                    facet_condition['filter']['bool']['must_not']['bool']['should'].append({
                        "term": {
                            facet_name_2_field(selection['facet_name']): bucket
                        }
                    })
            elif "Percentiles" == facet_name_2_facet_kind(selection["facet_name"]):
                facet_condition = {
                    "filter": {
                        "bool": {
                            "must_not": [
                                {
                                    "range": {
                                        facet_name_2_field(selection['facet_name']): {
                                            "gte": selection["range"][0],
                                            "lte": selection["range"][1]
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "weight": 0
                }
            elif "Date histogram" == facet_name_2_facet_kind(selection["facet_name"]):
                switcher = {
                    "LAST_MONTH": 30,
                    "LAST_YEAR": 365,
                    "LAST_3_YEARS": 365 * 3,
                    "LAST_5_YEARS": 365 * 5
                }
                qty_of_days = switcher.get(selection["bucket"], 30)
                facet_condition = {
                    "filter": {
                        "bool": {
                            "must_not": [
                                {
                                    "range": {
                                        facet_name_2_field(selection['facet_name']): {
                                            "gte": "now-" + str(qty_of_days) + "d/d"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "weight": 0
                }
            self.body['query']['function_score']["functions"].append(facet_condition)
        # Process facet 2 lv
        indices_wildcard = ""
        for facet_1lv in list_selected_facets_in_facet_2lv:
            for bucket in facet_1lv.get("buckets", []):
                indices_wildcard += DocumentSeeker.hash_map_facet_name_2_esindex_name["category"][
                                        facet_1lv["facet_name"]] + \
                                    "_" + \
                                    DocumentSeeker.hash_map_facet_name_2_esindex_name["type"][bucket] + \
                                    "*,"
        indices_wildcard = indices_wildcard.strip(",")
        if indices_wildcard == "":
            return "*"
        else:
            return indices_wildcard

    def __add_sort_demands_into_body(self):
        def sort_feature_2_key(sort_feature):
            switcher = {
                "RELEASE_DATE": "created_day"
            }
            return switcher.get(sort_feature, "created_day")

        list_sort_demands = []
        for sort_selection in self.sort_selections:
            list_sort_demands.append({
                sort_feature_2_key(sort_selection['feature']): {
                    "order": "desc" if sort_selection['descending'] else "asc"
                }
            })
        if len(list_sort_demands) > 0:
            self.body['sort'] = list_sort_demands

    def search_and_generate_facets(self):
        self.__build_basic_body()
        self.__add_facets_generator_into_body()
        valid_body = dsl_dto.GeneralDSL(**self.body).dict(by_alias = True, exclude_unset= True) # validate
        self.body = valid_body
        # Convert to Search object
        search_obj = Search(index=self.category + "_*" + self.selected_type + "*").update_from_dict(self.body)
        log("This is search obj:\n {}".format(search_obj))
        search_results, total_docs = search_obj.execute(), search_obj.count()
        list_docs, facets = self.__process_search_result(search_results)
        return total_docs, list_docs, facets

    def search_and_filter_sort(self):
        self.__build_basic_body()
        indices_wildcard = self.__add_facet_demands()
        self.__add_sort_demands_into_body()
        valid_body = dsl_dto.GeneralDSL(**self.body).dict(by_alias = True, exclude_unset= True) # validate
        self.body = valid_body
        # Convert to Search object
        search_obj = Search(index=indices_wildcard).update_from_dict(self.body)
        log("This is search obj:\n {}".format(search_obj))
        search_results = search_obj.execute()
        total_docs = search_obj.count()
        list_docs, _ = self.__process_search_result(search_results)
        return total_docs, list_docs

    def search_best_quality(self):
        body = {
            "_source": list_returned_keys,
            "from": self.from_index, "size": self.size,
            "sort": [
                {"quality": {"order": "desc"}},
                {"views": {"order": "desc"}}
            ],
            "query": {
                "function_score": {
                    # Match all
                    "query": {
                        "match_all": {}
                    },
                    "functions": [
                        # Consider lv_owner
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Beginner",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.3
                        },
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Intermediate",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.2
                        }
                    ],
                    "score_mode": "multiply",
                    "boost_mode": "multiply",
                }
            }
        }
        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result, total_docs = search_obj.execute(), search_obj.count()
        list_docs, _ = self.__process_search_result(search_result)
        return total_docs, list_docs

    def search_newest(self):
        """temporarily, it's deprecated"""
        body = {
            "_source": list_returned_keys,
            "from": self.from_index, "size": self.size,
            "query": {
                "function_score": {
                    "gauss": {
                        "created_day": {
                            "scale": "30d",
                            "offset": "7d",
                            "decay": 0.4
                        }
                    }
                }
            }
        }

        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result, total_docs = search_obj.execute(), search_obj.count()
        list_docs, _ = self.__process_search_result(search_result)
        return total_docs, list_docs

    def search_most_viewed(self):
        body = {
            "_source": list_returned_keys,
            "from": self.from_index, "size": self.size,
            "sort": [
                {"views": {"order": "desc"}},
                {"quality": {"order": "desc"}}
            ],
            "query": {
                "function_score": {
                    # Match all
                    "query": {
                        "match_all": {}
                    },
                    "functions": [
                        # Consider lv_owner
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Beginner",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.3
                        },
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Intermediate",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.2
                        }
                    ],
                    "score_mode": "multiply",
                    "boost_mode": "multiply"
                }
            }
        }

        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result, total_docs = search_obj.execute(), search_obj.count()
        list_docs, _ = self.__process_search_result(search_result)
        return total_docs, list_docs

    def search_by_url(self, url):
        """
        only for elearning program, youtube channel
        :return: a dict represent for a document
        """
        body = {
            "_source": list_returned_keys,
            "from": 0, "size": 1,
            "query": {
                "term": {
                    "url.keyword": url
                }
            }
        }
        # Convert to Search object
        search_obj = Search(index="elearning_program*,youtube_channel").update_from_dict(body)
        search_result = search_obj.execute()
        list_docs, _ = self.__process_search_result(search_result)
        if len(list_docs) == 1:
            return list_docs[0]
        else:
            return {}
