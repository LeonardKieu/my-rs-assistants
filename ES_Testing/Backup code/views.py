from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from document import search
from document.serializer.document_serializer import *


def health_check(request):
    return HttpResponse('It works!')


@api_view(['GET'])
def resolve_document(request: Request, *args, **kwargs) -> Response:
    if request.method == 'GET':
        # If user didn't provide text, match all
        search_text = request.GET.get('text', 'Machine Learning Deep Python AI Data Science Mathematics')
        # Pagination
        size = int(request.GET.get('pageSize', 10))
        current_page = int(request.GET.get('currentPage', 1))
        from_index = (current_page - 1) * size
        # Pass via body
        selected_facets = request.data.get("selected_facets", None)
        sort_selections = request.data.get("sort_selections", None)

        # Instantiate a Document_Seeker obj
        seeker = search.DocumentSeeker(search_text=search_text,
                                       from_index=from_index, size=size,
                                       selected_facets=selected_facets, sort_selections=sort_selections)
        # Process with ES
        if selected_facets or sort_selections:
            total_docs, list_docs = seeker.search_and_filter_sort()
            data = {
                'total_docs': total_docs,
                'matched_docs': list_docs
            }
        else:
            total_docs, list_docs, facets = seeker.search_and_generate_facets()
            data = {
                'total_docs': total_docs,
                'matched_docs': list_docs,
                'facets': facets
            }
        response = SearchResult(instance=data)
        return Response(data=response.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def resolve_best_quality(request: Request, *args, **kwargs) -> Response:
    # Pagination
    current_page = int(request.GET.get('currentPage', 1))
    size = int(request.GET.get('pageSize', 10))
    from_index = (current_page - 1) * size

    # Instantiate a DocumentSeeker obj and use
    seeker = search.DocumentSeeker(from_index=from_index, size=size)
    total_docs, list_docs = seeker.search_best_quality()

    data = {
        'total_docs': total_docs,
        'matched_docs': list_docs
    }

    response = SearchResult(instance=data)
    return Response(data=response.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def resolve_newest(request: Request, *args, **kwargs) -> Response:
    # Pagination
    current_page = int(request.GET.get('currentPage', 1))
    size = int(request.GET.get('pageSize', 10))
    from_index = (current_page - 1) * size

    # Instantiate a DocumentSeeker obj and use
    seeker = search.DocumentSeeker(from_index=from_index, size=size)
    total_docs, list_docs = seeker.search_newest()

    data = {
        'total_docs': total_docs,
        'matched_docs': list_docs
    }

    response = SearchResult(instance=data)
    return Response(data=response.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def resolve_most_viewed(request: Request, *args, **kwargs) -> Response:
    # Pagination
    current_page = int(request.GET.get('currentPage', 1))
    size = int(request.GET.get('pageSize', 10))
    from_index = (current_page - 1) * size

    # Instantiate a DocumentSeeker obj and use
    seeker = search.DocumentSeeker(from_index=from_index, size=size)
    total_docs, list_docs = seeker.search_most_viewed()

    data = {
        'total_docs': total_docs,
        'matched_docs': list_docs
    }
    response = SearchResult(instance=data)
    return Response(data=response.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def resolve_bestquality_newest_mostviewed(request: Request, *args, **kwargs) -> Response:
    # Get param
    best_quality_qty = request.GET.get('bestQualityQty', 2)
    newest_qty = request.GET.get('newestQty', 2)
    most_viewed_qty = request.GET.get('mostViewedQty', 2)

    # Instantiate a DocumentSeeker obj and use
    total_docs, list_best_quality = search.DocumentSeeker(from_index=0,
                                                          size=best_quality_qty).search_best_quality()
    total_docs, list_newest = search.DocumentSeeker(from_index=0,
                                                    size=newest_qty).search_newest()
    total_docs, list_most_viewed = search.DocumentSeeker(from_index=0,
                                                         size=most_viewed_qty).search_most_viewed()

    data = {
        'best_quality_docs': list_best_quality,
        'newest_docs': list_newest,
        'most_viewed_docs': list_most_viewed
    }

    response = TripleSearchResult(instance=data)
    return Response(data=response.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def resolve_get_doc_by_url(request: Request, *args, **kwargs):
    # Get param
    url = request.GET.get('url', '*')
    # Instantiate a DocumentSeeker obj and use
    dict_doc = search.DocumentSeeker().search_by_url(url)
    if dict_doc == {}:
        response = Document(data={"info": "Not found!"})
        response.is_valid()
        return Response(data=response.data, status=status.HTTP_404_NOT_FOUND)
    else:
        data = {}
        for prop in list_returned_keys:
            if prop in dict_doc: data[prop] = dict_doc[prop]
        response = Document(data=data)
        response.is_valid()
        return Response(data=response.data, status=status.HTTP_200_OK)
