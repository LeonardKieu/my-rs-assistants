from elasticsearch_dsl import connections, Search, FacetedSearch, TermsFacet, DateHistogramFacet, RangeFacet, A
from elasticsearch_dsl.query import MultiMatch, MatchAll
from elasticsearch_dsl.faceted_search import FacetedResponse
# from document.serializer.document_serializer import *

# from .serializer.document_serializer import list_returned_keys
list_returned_keys = ['title', 'language', 'quality']

import sys, json


def log(text=None):
    print(text, file=sys.stderr)


def print_search_obj(s):
    dict = s.to_dict()
    log("This is search obj (Dict):")
    json_obj = json.dumps(dict, indent=4, ensure_ascii=False)
    log(json_obj)


connections.create_connection(hosts=['localhost'], timeout=2000)


class DocumentSeekerAssistant:
    __hash_map_category_types = {  # in data
        "E_LEARNING": ["COURSE", "PROGRAM"],
        "BOOK": ["GENERAL", "SCI_TECH", "SCIENCE", "TECHNICAL"],
        "YOUTUBE": ["PLAYLISTS", "CHANNEL"]
    }
    __facet_kind = {
        "Terms": [
            # CATEGORY, TYPE in Facet 2 Lv
            "LANGUAGE", "SKILL_LEVEL"
        ],
        "Percentiles": ["RATING"],
        "Date histogram": ["RELEASE_DATE"]
    }

    def __init__(self,
                 search_text=None,
                 page_size=10, current_page=1,
                 selected_facets: List[FacetRequestDto] = None,
                 selected_sorts: List[SortRequestDto] = None):
        self.__search_text = search_text

        self.__page_size = page_size  # ES syntax: "size"
        self.__current_page = current_page
        self.pagination_start = (self.__current_page - 1) * self.__page_size  # ES syntax: "from"
        self.pagination_stop = self.pagination_start + self.__page_size

        self.selected_facets = selected_facets
        self.selected_sorts = selected_sorts

    @classmethod
    def __process_search_result(cls, search_result: FacetedResponse):
        def type_to_doc_count(type):
            for bk in search_result.aggregations.TYPE.buckets:
                if bk.key == type: return bk.doc_count

        def category_to_doc_count(category):
            for bk in search_result.aggregations.CATEGORY.buckets:
                if bk.key == category: return bk.doc_count

        # Process for total_docs
        total_docs = search_result.hits.total.value

        # Process for list docs
        list_docs = []
        for hit in search_result['hits']['hits']:
            doc_id = hit['_id']
            hit = hit['_source']
            doc = {}
            # Get key if it exists in hit and bind into doc
            for key in list_returned_keys:
                if hasattr(hit, key):
                    doc[key] = hit[key]
            doc['id'] = doc_id
            list_docs.append(doc)

        # Process for facets
        facets = []
        if search_result.aggregations:
            # Process Facet
            for facet_name, facet_val in search_result.aggregations.to_dict().items():
                # # Process facet "Term"
                if facet_name in DocumentSeekerAssistant.__facet_kind["Terms"]:
                    facet = {}
                    facet['name'] = facet_name
                    buckets = []
                    for raw_bucket in list(facet_val['buckets']):
                        bucket = {}
                        bucket['name'] = raw_bucket['key']
                        bucket['doc_count'] = raw_bucket['doc_count']
                        buckets.append(bucket)
                    facet['buckets'] = buckets
                    facets.append(facet)
                # # Process facet "Percentiles"
                elif facet_name in DocumentSeekerAssistant.__facet_kind["Percentiles"]:
                    facet = {}
                    facet['name'] = facet_name
                    buckets = []
                    for bucket_name, val in dict(facet_val['values']).items():
                        bucket = {}
                        bucket['name'] = "START" if "0.0" == bucket_name else "STOP"
                        bucket['doc_count'] = val
                        buckets.append(bucket)
                    facet['buckets'] = buckets
                    facets.append(facet)
                # # Process facet "Date histogram"
                elif facet_name in DocumentSeekerAssistant.__facet_kind["Date histogram"]:
                    print("Date hist processing...")
            # Process Facet containing SubFacet
            hash_map_category_types = DocumentSeekerAssistant.__hash_map_category_types.copy()
            list_existed_categories = [bucket.key for bucket in search_result.aggregations.CATEGORY.buckets]
            list_existed_types = [bucket.key for bucket in search_result.aggregations.TYPE.buckets]
            for business_category in DocumentSeekerAssistant.__hash_map_category_types.keys():
                if business_category not in list_existed_categories:
                    hash_map_category_types.pop(business_category, None)
                else:
                    for business_type in DocumentSeekerAssistant.__hash_map_category_types[business_category]:
                        if business_type not in list_existed_types:
                            hash_map_category_types[business_category].remove(business_type)
            facet_category = {}
            facet_category['name'] = "CATEGORY"
            buckets = []
            for category in hash_map_category_types.keys():
                sub_facet = {}
                sub_facet['name'] = category
                sub_buckets = []
                for type in hash_map_category_types[category]:
                    bucket = {}
                    bucket['name'] = type
                    bucket['doc_count'] = type_to_doc_count(type)
                    sub_buckets.append(bucket)
                sub_facet['buckets'] = sub_buckets
                sub_facet['sum_buckets'] = category_to_doc_count(category)
                buckets.append(sub_facet)
            facet_category['buckets'] = buckets
            facets.append(facet_category)

        # Process sorts
        sorts = ["RELEASE_DATE"]
        return total_docs, list_docs, facets, sorts

    def search_and_generate_facets(self):
        # Transform search_text, page_size, current_page to feed DocumentSeeker
        response = DocumentSeeker(query=self.__search_text,
                                  pagination_start=self.pagination_start,
                                  pagination_stop=self.pagination_stop).execute()
        # Get the response and __process_search_result, return total_docs, list_docs, facets, sorts
        return DocumentSeekerAssistant.__process_search_result(response)

    def __get_aliases_wildcard(self):
        categories_buckets = []
        for facet in self.selected_facets:
            if "CATEGORY" == facet.name:
                categories_buckets = facet.buckets
                break
        indices_wildcard = ""
        for category in categories_buckets:
            for type in category.buckets:
                indices_wildcard += category.name + "-" + type + "*,"
        indices_wildcard = indices_wildcard.strip(",")
        return "*" if indices_wildcard == "" else indices_wildcard

    def search_and_filter_sort(self):
        # selected_facets => indices_wildcard, custom_filters, sort
        indices_wildcard = self.__get_aliases_wildcard()
        custom_filters = {}
        for facet in self.selected_facets:
            if "CATEGORY" != facet.name:
                custom_filters[facet.name] = facet.buckets
        customized_sort = []
        for sort in self.selected_sorts:
            switcher = {
                "RELEASE_DATE": "created_day"
            }
            customized_sort.append(
                "-" + switcher.get(sort.feature, "created_day") if "DESC" == sort.order else switcher.get(sort.feature,
                                                                                                          "created_day"))
        customized_sort = tuple(customized_sort)
        # Feed to DocumentSeeker full args
        response = DocumentSeeker(indices_wildcard=indices_wildcard,
                                  query=self.__search_text,
                                  customized_filters=custom_filters,
                                  customized_sort=customized_sort,
                                  pagination_start=self.pagination_start,
                                  pagination_stop=self.pagination_stop).execute()
        # Get Response and __process_search_result, return total_docs, list_docs
        total_docs, list_docs, _, _ = DocumentSeekerAssistant.__process_search_result(response)
        return total_docs, list_docs

    def search_best_quality(self):
        body = {
            "_source": list_returned_keys,
            "from": self.pagination_start, "size": self.__page_size,
            "sort": [
                {"quality": {"order": "desc"}},
                {"views": {"order": "desc"}}
            ],
            "query": {
                "function_score": {
                    # Match all
                    "query": {
                        "match_all": {}
                    },
                    "functions": [
                        # Consider lv_owner
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Beginner",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.3
                        },
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Intermediate",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.2
                        }
                    ],
                    "score_mode": "multiply",
                    "boost_mode": "multiply",
                }
            }
        }
        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result = search_obj.execute()
        total_docs, list_docs, _, _ = DocumentSeekerAssistant.__process_search_result(search_result)
        return total_docs, list_docs

    def search_newest(self):
        """temporarily, it's deprecated"""
        body = {
            "_source": list_returned_keys,
            "from": self.pagination_start, "size": self.__page_size,
            "query": {
                "function_score": {
                    "gauss": {
                        "created_day": {
                            "scale": "30d",
                            "offset": "7d",
                            "decay": 0.4
                        }
                    }
                }
            }
        }

        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result = search_obj.execute()
        total_docs, list_docs, _, _ = DocumentSeekerAssistant.__process_search_result(search_result)
        return total_docs, list_docs

    def search_most_viewed(self):
        body = {
            "_source": list_returned_keys,
            "from": self.pagination_start, "size": self.__page_size,
            "sort": [
                {"views": {"order": "desc"}},
                {"quality": {"order": "desc"}}
            ],
            "query": {
                "function_score": {
                    # Match all
                    "query": {
                        "match_all": {}
                    },
                    "functions": [
                        # Consider lv_owner
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Beginner",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.3
                        },
                        {
                            "filter": {
                                "multi_match": {
                                    "query": "Intermediate",
                                    "analyzer": "my_english_analyzer",
                                    "fields": [
                                        "level_owner.keyword"
                                    ]
                                }
                            },
                            "weight": 1.2
                        }
                    ],
                    "score_mode": "multiply",
                    "boost_mode": "multiply"
                }
            }
        }

        # Convert to Search object
        search_obj = Search(index="*").update_from_dict(body)
        search_result = search_obj.execute()
        total_docs, list_docs, _, _ = DocumentSeekerAssistant.__process_search_result(search_result)
        return total_docs, list_docs

    @classmethod
    def search_by_url(cls, url):
        """
        only for elearning program, youtube channel
        :return: a dict represent for a document
        """
        body = {
            "_source": list_returned_keys,
            "from": 0, "size": 1,
            "query": {
                "term": {
                    "url.keyword": url
                }
            }
        }
        # Convert to Search object
        search_obj = Search(index="elearning_program*,youtube_channel").update_from_dict(body)
        search_result = search_obj.execute()
        _, list_docs, _, _ = DocumentSeekerAssistant.__process_search_result(search_result)
        return list_docs[0] if len(list_docs) == 1 else {}


class DocumentSeeker(FacetedSearch):
    fields = [
        "title^10",
        "category^10",
        "type^10",
        "author^5",
        "keyword^5",
        "level^5",
        "level_owner^5",
        "language^5",
        "description",
        "review",
        "resource^10",
        "offered^10",
        "publisher",
        "title_program",
        "title_channel",
        # Related keys
        # elearning_program_coursera
        "knowledge",
        "search_key",
        "skill",
        # elearning_program_edx
        "jobOutLook",
        # elearning_course_edx
        "prerequisites",
        "associated_programs.name",
        "subject.name",
    ]
    pagination_start = 0
    pagination_stop = 10

    def __init__(self,
                 indices_wildcard="_all",
                 query=None,
                 customized_filters={}, customized_sort=(),
                 pagination_start=0, pagination_stop=10):
        DocumentSeeker.index = indices_wildcard
        if customized_filters != {}:
            DocumentSeeker.facets = {
                # CATEGORY, TYPE should be handled by generating indices wildcard
                "SKILL_LEVEL": TermsFacet(field='enum_level_owner.keyword', size=5),
                "RATING": RangeFacet(field="quality",
                                     ranges=[("need_range",
                                              (customized_filters["RATING"][0],
                                               customized_filters["RATING"][1] + 0.001))]),
                "LANGUAGE": TermsFacet(field='enum_language.keyword', size=5),
                "RELEASE_DATE": DateHistogramFacet(field='created_day', interval='quarter')  # ???
            }
            customized_filters["RATING"] = ["need_range"]
        super().__init__(query=query, filters=customized_filters, sort=customized_sort)
        DocumentSeeker.pagination_start = pagination_start
        DocumentSeeker.pagination_stop = pagination_stop

    def query(self, search, query):
        """
        Coerce main query using custom analyzer.
        Run before aggregate method\n
        :param search: original search from parent class
        :param query: the text to search for
        :return: search obj
        """
        main_query = MultiMatch(query=query,
                                analyzer="my_english_analyzer",
                                fields=self.fields) if query else MatchAll()
        search.query = main_query
        return search.source(list_returned_keys)[DocumentSeeker.pagination_start:DocumentSeeker.pagination_stop]

    def highlight(self, search):
        list_highlight_key = ['description', 'title']
        highlight_search = search
        for highlight_key in list_highlight_key:
            highlight_search = highlight_search.highlight(highlight_key)
        return highlight_search

    def aggregate(self, search):
        list_aggregations = [
            ["CATEGORY", A("terms", field='enum_category.keyword', size=10)],
            ["TYPE", A("terms", field='enum_type.keyword', size=30)],
            ["SKILL_LEVEL", A("terms", field='enum_level_owner.keyword', size=5)],
            ["RATING", A("percentiles", field="quality", percents=[0, 100])],
            ["LANGUAGE", A("terms", field='enum_language.keyword', size=5)],
            ["RELEASE_DATE", A("date_histogram", field="created_day", calendar_interval="quarter", format="MM-dd-yyyy")]
            # ???
        ]
        for agg in list_aggregations:
            search.aggs.bucket(agg[0], agg[1])
        log("After query, highlight method, in agg method:")
        print_search_obj(search)
        return search


if __name__ == "__main__":
    query = "Deep learning and tensorflow"
    custom_filters = {
        "SKILL_LEVEL": ["BEGINNER"],
        "LANGUAGE": ["ENGLISH", "SPANISH"],
        "RATING": [3, 5],
        # "RELEASE_DATE": date(2020, 3, 27)#???
    }

    sort = ["-created_day"]
    search_obj = DocumentSeeker(query=query, pagination_start=0,
                                pagination_stop=10)#, customized_filters=custom_filters, customized_sort=sort)
    res = search_obj.execute()
    print("Response:\n{}".format(type(res)))
    print("Total doc:\n{}".format(res.hits.total))
    print("Print some of titles:")
    for hit in res[:2]:
        print(hit.title)
        print(hit.language)
    # print("Test facet:\n")
    # print(res.facets.tags)
    # print("Facets:\n{}".format(res.facets))
    # for facet in res.facets:
    #     print(facet)

    # print("Test assistant:")
    # assistant = DocumentSeekerAssistant(search_text=query, page_size=5, current_page=2)
    # total_docs, list_docs, facets, sorts = assistant.search_and_generate_facets()
    # print("Total: {}\nList doc:\n {}\nList facets:\n {}\nSorts: {}".format(total_docs, list_docs[:2], facets, sorts))
