import json
from elasticsearch_dsl import Search, connections
import re
import pymongo
from pymongo import MongoClient
# from document import *

# This connection work with warning "Insecure..."
connections.create_connection(hosts=['https://localhost:9200'], timeout=2000, use_ssl=False, verify_certs=False, verify=False)
# This connection doesn't work
# connections.create_connection(hosts=['localhost:9200'], timeout=2000)

def process_search_result(search_result):
    list_docs = []
    for hit in search_result['hits']['hits']:
        doc_id = hit['_id']
        hit = hit['_source']
        doc = Document()
        # Get key if it exists in hit and bind into doc
        for key in list_returned_keys:
            if hasattr(hit, key):
                setattr(doc, key, hit[key])
        setattr(doc, 'id', doc_id)
        list_docs.append(doc)
    return list_docs

def get_script_score(category = "*", selected_type = "*", resource = "*", list_keys = []):
    """
    Param:
    category, selected_type, resource: choose which ES indices will be calculated aggregations (sum)
    list_keys: list key for agg (sum) list results
    """
    # Prepare request
    body = {
        "aggs": {
            # Calc aggregations here
        }
    }

    for key in list_keys:
        body["aggs"]["total_" + key] = {
            "sum": {
                "field": key
            }
        }

    # print(json.dumps(body, indent=4)) # test

    # Using Search object
    if resource != "*":
        resource = "*_" + resource
    # print(category + "_*" + selected_type + resource) # test
    s = Search(index=category + "_*" + selected_type + resource).update_from_dict(body)
    response = s.execute()

    # Get results and convert to scrip_score
    script_score = "doc['quality'].value"
    for key in list_keys:
        sum_of_key = response['aggregations']["total_" + key]['value']
        script_score += " + (doc['"+key+"'].value / " + str(sum_of_key) + ")"

    return script_score

def search_document(search_text = "*", category = "*", selected_type = "*", from_index = 0, size = 10):
    body = {
        "_source": list_returned_keys,
        "from": from_index, "size": size,
        "query": {
            "function_score": {
                # stage 1
                "query": {
                    # Whether it exists a search text?
                },
                # stage 2
                "functions": [
                    # 3 funcs here if it exists search text
                    # List of Business here
                ],
                "score_mode": "multiply",
                "boost_mode": "multiply"
            }
        }
        # Generate Facets by aggs
    }
    # List of Business
    # category, type, resource => choose the scope of indices
    # "in_data": the conditions for filtering in func_score
    list_dicts_business = [
        # Youtube
        {
            "category": "youtube",
            "type": "playlist",
            "resource": "*",
            "category_in_data": "Youtube",
            "type_in_data": "Playlists",
            "keys": ["like", "views"]
        },
        {
            "category": "youtube",
            "type": "channel",
            "resource": "*",
            "category_in_data": "Youtube",
            "type_in_data": "Channel",
            "keys": ["calculated_follow", "views"]
        },
        # Elearning
        {
            "category": "elearning",
            "type": "course",
            "resource": "coursera",
            "category_in_data": "E-Learning",
            "type_in_data": "Course",
            "resource_in_data": "Coursera",
            "keys": ["member_rating", "views"]
        },
        {
            "category": "elearning",
            "type": "program",
            "resource": "coursera",
            "category_in_data": "E-Learning",
            "type_in_data": "Program",
            "resource_in_data": "Coursera",
            "keys": ["member", "views"]
        },
        {
            "category": "elearning",
            "type": "course",
            "resource": "edx",
            "category_in_data": "E-Learning",
            "type_in_data": "Course",
            "resource_in_data": "Edx",
            "keys": ["member", "calculated_time"]
        },
        {
            "category": "elearning",
            "type": "program",
            "resource": "edx",
            "category_in_data": "E-Learning",
            "type_in_data": "Program",
            "resource_in_data": "Edx",
            "keys": ["calculated_time", "calculated_fee"]
        },
        {
            "category": "elearning",
            "type": "course",
            "resource": "datacamp",
            "category_in_data": "E-Learning",
            "type_in_data": "Course",
            "resource_in_data": "datacamp",
            "keys": ["member", "exp"]
        },
        # Book
        {
            "category": "book",
            "type": "general",
            "resource": "amazon",
            "category_in_data": "Book",
            "type_in_data": "General",
            "resource_in_data": "Amazon",
            "keys": ["member_rating"]
        },
        {
            "category": "book",
            "type": "technical",
            "resource": "amazon",
            "category_in_data": "Book",
            "type_in_data": "Technical",
            "resource_in_data": "Amazon",
            "keys": ["member_rating"]
        },
        {
            "category": "book",
            "type": "science",
            "resource": "amazon",
            "category_in_data": "Book",
            "type_in_data": "Science",
            "resource_in_data": "Amazon",
            "keys": ["member_rating"]
        },
        {
            "category": "book",
            "type": "science_technical",
            "resource": "amazon",
            "category_in_data": "Book",
            "type_in_data": "Sci - Tech",
            "resource_in_data": "Amazon",
            "keys": ["member_rating"]
        }
    ]

    # Prune List of Business
    if category != "*":
        list_dicts_business[:] = [d for d in list_dicts_business if d['category'] == category]
    if selected_type != "*":
        list_dicts_business[:] = [d for d in list_dicts_business if re.search(selected_type, d['type'])]

    # Process business
    for b in list_dicts_business:
        added_func = {
            "filter": {
                "bool": {
                    "must": [
                        # Exist quality and keys in business list
                        {
                            "exists": {
                                "field": "quality"
                            }
                        },
                        ## Add keys in business list here
                        # Match exactly category, type, resource in data
                        {
                            "term": {
                                "category.keyword": b['category_in_data']
                            }
                        },
                        {
                            "term": {
                                "type.keyword": b['type_in_data']
                            }
                        }
                        ## Match key resource in data (if it exists) here
                    ]
                }
            },
            "script_score": {
                "script": {
                    # Using the scope (category, type, resource) to calc and generate script_score
                    "source": get_script_score(category=b['category'], selected_type=b['type'], resource=b['resource'],
                                               list_keys=b['keys'])
                }
            }
        }
        # Add keys in business list here
        for key in b["keys"]:
            existing_key = {
                "exists": {
                    "field": key
                }
            }
            added_func["filter"]["bool"]["must"].append(existing_key)
        # Add resource if it exists
        if "resource_in_data" in b:
            matched_resource = {
                "term": {
                    "resource.keyword": b['resource_in_data']
                }
            }
            added_func["filter"]["bool"]["must"].append(matched_resource)
        body["query"]["function_score"]["functions"].append(added_func)

    if search_text == "*":
        body['query']['function_score']['query'] = {"match_all": {}}
    else:
        body['query']['function_score']['query'] = {
            "multi_match": {
                "query": search_text,
                "analyzer": "my_english_analyzer",
                "fields": [
                    # Display on FE
                    "title",
                    "category",
                    "category.keyword",
                    "type",
                    "type.keyword",
                    "author",
                    "author.keyword",
                    "keyword",
                    "keyword.keyword",
                    "level",
                    "level.keyword",
                    "level_owner",
                    "level_owner.keyword",
                    "language",
                    "description",
                    "review",
                    "resource",
                    "resource.keyword",
                    "offered",
                    "publisher",
                    "publisher.keyword",
                    "title_program",
                    "title_channel",
                    # Related keys
                    # elearning_program_coursera
                    "knowledge",
                    "search_key",
                    "skill",
                    # elearning_program_edx
                    "jobOutLook",
                    # elearning_course_edx
                    "prerequisites",
                    "associated_programs.name",
                    "subject.name",
                    # youtube_channel
                    "sub.episodes.name",
                    "sub.playlists.name",
                    "sub.videos.name",
                    # elearning_course_coursera
                    "sub.description",
                    "sub.name",
                    "sub.practice.name",
                    "sub.reading.name",
                    "sub.video.name",
                    # elearning_course_datacamp
                    "sub.description",
                    "sub.title",
                    "sub.video.name"
                ],
                "fuzziness": 1
            }
        }
        key_sets_5 = {
            "filter": {
                "multi_match": {
                    "query": search_text,
                    "analyzer": "my_english_analyzer",
                    "fields": [
                        "keyword",
                        "author",
                        "level",
                        "level_owner",
                        "language"
                    ]
                }
            },
            "weight": 5
        }
        key_sets_10 = {
            "filter": {
                "multi_match": {
                    "query": search_text,
                    "analyzer": "my_english_analyzer",
                    "fields": [
                        "category",
                        "type",
                        "title",
                        "resource",
                        "offered"
                    ]
                }
            },
            "weight": 10
        }
        title_regex = {
            "filter": {
                "regexp": {
                    "title.keyword": {
                        "value": ".*" + re.escape(search_text) + ".*",
                        "flags": "ALL"
                    }
                }
            },
            "weight": 1000
        }
        body['query']['function_score']['functions'].insert(0, key_sets_5)
        body['query']['function_score']['functions'].insert(0, key_sets_10)
        body['query']['function_score']['functions'].insert(0, title_regex)

    # Generate Facets
    aggregator = search_engine_business.Faceted_Search_Business()
    if category != "*": aggregator.add_facets(category)
    body['aggs'] = aggregator.facets
    # Convert to Search object
    search_obj = Search(index=category + "_*" + selected_type + "*").update_from_dict(body)
    return search_obj.execute(), search_obj.count()

def measure_pace_of_search_engine(search_text = "*", category = "*", selected_type = "*", from_index = 0, size = 10):
    info, total = search_document(search_text, category, selected_type, from_index, size)
    print("-"*100)
    print("Search engine 1 gets totally {} docs in {} ms".format(total, info['took']))

search_text = "Data Analytics For Businesses 2019: How to Master Data Science with Optimized Marketing Strategies using Data Mining Algorithms, Big Data for Business and Machine Learning"
category = "*"
selected_type = "*"
resource = "*"
list_keys = ['like', 'views']
from_index = 0
size = 10

# get_script_score(lis)
measure_pace_of_search_engine(search_text, category, selected_type, from_index, size)
# import string
# print(search_text.translate(str.maketrans('', '', string.punctuation)))
# print(re.escape("Business (Data"))
