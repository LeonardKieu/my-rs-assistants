from elasticsearchcourier.ElasticsearchCourier import ElasticsearchCourier

courier = ElasticsearchCourier()
list_indices = [
    'elearning_program_coursera',  #
    'elearning_program_edx',  #
    'elearning_course_coursera',  #
    'elearning_course_edx',  #
    'elearning_course_datacamp',  #
    'book_general_amazon',  #
    'book_science_amazon',  #
    'book_science_technical_amazon',  #
    'book_technical_amazon',  #
    'youtube_channel',  #
    'youtube_playlist' #
]
for index in list_indices:
    url_of_json = "https://d3jvm38gqovq64.cloudfront.net/documents/{}.json".format(index)
    print("From url {}".format(url_of_json))
    courier.import_into_es(url_of_json, index)
