

TOLERANCE = 4

HEADERS = {
    'Content-Type': 'application/json',
    'Cookie': 'csrftoken=MZ0PhOxkUZkP0SVGXw8dhJtDGlgIrn1gJqDzMRDajPZy8xVQ5XAoK5DHNOpYXbrS'
}

ANALYZER_CONFIG = {
    "settings": {
        "analysis": {
            "filter": {
                "english_stop": {
                    "type": "stop",
                    "stopwords": "_english_"
                }
            },
            "analyzer": {
                "my_english_analyzer": {
                    "tokenizer": "standard",
                    "filter": [
                        "lowercase",
                        "english_stop"
                    ]
                }
            }
        }
}
}

# (Temporarily) Config special mapping for sci_tech, sci, etc...
INDICES_MAPPING_CONFIG = {
    "book_science_technical_amazon": {
        "properties": {
            "quality": {
                "type": "float"
            }
        }
    },
    "book_science_amazon": {
        "properties": {
            "quality": {
                "type": "float"
            }
        }
    }
}

INDICES_ALIAS = {
    "actions": [
        {
            "add": {
                "index": "elearning_program_coursera",
                "alias": "E_LEARNING-PROGRAM-COURSERA"
            }
        },
        {
            "add": {
                "index": "elearning_program_edx",
                "alias": "E_LEARNING-PROGRAM-EDX"
            }
        },
        {
            "add": {
                "index": "youtube_channel",
                "alias": "YOUTUBE-CHANNEL"
            }
        },
        {
            "add": {
                "index": "book_general_amazon",
                "alias": "BOOK-GENERAL-AMAZON"
            }
        },
        {
            "add": {
                "index": "elearning_course_edx",
                "alias": "E_LEARNING-COURSE-EDX"
            }
        },
        {
            "add": {
                "index": "book_science_technical_amazon",
                "alias": "BOOK-SCI_TECH-AMAZON"
            }
        },
        {
            "add": {
                "index": "elearning_course_coursera",
                "alias": "E_LEARNING-COURSE-COURSERA"
            }
        },
        {
            "add": {
                "index": "book_technical_amazon",
                "alias": "BOOK-TECHNICAL-AMAZON"
            }
        },
        {
            "add": {
                "index": "youtube_playlist",
                "alias": "YOUTUBE-PLAYLISTS"
            }
        },
        {
            "add": {
                "index": "book_science_amazon",
                "alias": "BOOK-SCIENCE-AMAZON"
            }
        },
        {
            "add": {
                "index": "elearning_course_datacamp",
                "alias": "E_LEARNING-COURSE-DATACAMP"
            }
        }
    ]
}