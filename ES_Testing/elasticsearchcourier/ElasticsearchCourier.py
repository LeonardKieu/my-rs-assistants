import requests
import argparse
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import json
from .settings import TOLERANCE, HEADERS, ANALYZER_CONFIG, INDICES_MAPPING_CONFIG, INDICES_ALIAS


class ElasticsearchCourier:

    def __init__(self, es_url="http://localhost:9200"):
        # Instantiate connection to ES Server
        self.__es_url = es_url
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "-H",
            "--host",
            action="store",
            default=es_url,
            help="The elasticsearch host you wish to connect to. (Default: localhost:9200)",
        )
        args = parser.parse_args()
        self.__es = Elasticsearch(args.host, use_ssl=False, verify_certs=False)

    @classmethod
    def transform_yielded_dict(cls, yielded_dict: dict):
        """
        Override this method to transform your dict
        :param yielded_dict: dict
        :return: dict
        """
        # yielded_dict["_id"] = str(yielded_dict["eventId"])
        yielded_dict["_id"] = str(yielded_dict["_id"]['$oid'])
        yielded_dict["created_day"] = yielded_dict["created_day"]["$date"]
        return yielded_dict

    @classmethod
    def __generate_es_doc(cls, raw_lst_dict, es_index_name):
        """
        Add several metadata into each json doc to index ES Server
        """
        for i in range(len(raw_lst_dict)):
            # Prepare returned obj - a dict in Py - a JSON document in ES
            yielded_dict = raw_lst_dict[i]

            # Add metadata to index into ES
            yielded_dict["_index"] = es_index_name
            yielded_dict["_type"] = "_doc"
            yielded_dict["_op_type"] = "index"

            # Process somes of special keys
            yielded_dict = ElasticsearchCourier.transform_yielded_dict(yielded_dict)

            yield yielded_dict

    def import_into_es(self, json_file_url, es_index_name="others"):
        """
            require json file format [{}, {},...]
        """
        info = {
            "successful": False,
            "failed_qty": 0,
            "tolerance": TOLERANCE
        }
        while info['successful'] is False and info['failed_qty'] < info['tolerance']:
            try:
                # On ES del available index that has same name
                response_del = requests.delete(self.__es_url + "/" + es_index_name)
                # print("Del :\n{}".format(response_del.status_code))

                # On ES, config analyzer
                response_conf_analyzer = requests.request("PUT", self.__es_url + "/" + es_index_name,
                                                          headers=HEADERS,
                                                          data=json.dumps(ANALYZER_CONFIG))
                # print("Conf analyzer :\n{}".format(response_conf_analyzer.status_code))

                # Config mapping
                mapping_config = INDICES_MAPPING_CONFIG.get(es_index_name, None)
                if mapping_config:
                    response_config_mapping = requests.request("PUT",
                                                               self.__es_url + "/" + es_index_name + "/" + "_mapping",
                                                               headers=HEADERS,
                                                               data=json.dumps(mapping_config))
                    # print("Special config for mapping:\n{}".format(response_config_mapping.status_code))

                # Index
                list_dict = requests.get(json_file_url).json()
                bulk(self.__es, ElasticsearchCourier.__generate_es_doc(list_dict, es_index_name), stats_only=True,
                     request_timeout=60000)

                # Set alias
                aliases_config = {
                    "actions": [setting for setting in INDICES_ALIAS["actions"] if
                                es_index_name == setting['add']['index']]
                }
                alias_response = requests.request("POST",
                                                  self.__es_url + "/_aliases",
                                                  headers=HEADERS,
                                                  data=json.dumps(aliases_config))
                # print("Config alias: \n{}".format(alias_response.status_code))

                info['successful'] = True
                print("Import data of {} successfully!".format(es_index_name))
            except Exception as e:
                info['failed_qty'] += 1
                print("Failed to import {}/{} time(s)!".format(info['failed_qty'], info["tolerance"]))
                print(e)
