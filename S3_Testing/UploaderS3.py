import boto3

class UploaderS3:
    def __init__(self):
        self.mimetype = 'image/jpeg' # you can programmatically get mimetype using the `mimetypes` module
        self.s3 = boto3.resource('s3').Bucket('staging-daita-public')
    def upload_img(self, category, type, doc_id, img_path):
        """
        params:\n
            category in ('youtube', 'course')\n
            type: according to the name of collection in each DB (category) in MongoDB\n
            doc_id: you inserted doc in MongoDB, it generates an _id automatically, fill that _id in this param\n
            img_path: the img path in your pc. You should save img in your pc and upload into S3 too.\n   
        return:\n
            doc_link         
        """
        try:
            self.s3.upload_file(img_path, '%s/%s/%s' % ('images/'+ category, type, doc_id), ExtraArgs={
                "ContentType": self.mimetype
            })
            return r'https://d3jvm38gqovq64.cloudfront.net/images/' + category + '/' + type + '/' + doc_id
        except Exception as e:
            print(e)
print(UploaderS3().upload_img('course', 'datacamp', 'x_sdf', r'C:\Users\pc\Downloads\Screen-Shot-2016-08-23-at-8.42.13-AM.jpg'))